from digitalio import DigitalInOut, Direction, Pull
from adafruit_tinylora.adafruit_tinylora import TTN, TinyLoRa
import busio
import board
import time


# SPI
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
cs = DigitalInOut(board.CE1)
irq = DigitalInOut(board.D5)
rst = DigitalInOut(board.D25)

devaddr = bytearray([0xc7, 0xd1, 0xd5, 0x45])
nwkey = bytearray([0xed, 0x58, 0x32, 0xfa, 0x18, 0x64, 0x70, 0xd2,
                   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

app = bytearray([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                 0x2e, 0xf7, 0x60, 0xef, 0x65, 0xd5, 0x23, 0x15])
ttn_config = TTN(devaddr, nwkey, app, country='ID')
lora = TinyLoRa(spi, cs, irq, rst, ttn_config)
lora.set_datarate("SF10BW125")
# str = "2022-08-05 16:39:33#4.3#0.2"
str = "12345678901234567890123456789012345678901234567890123456789012345678901234567890"
data_pkt = bytearray(str, 'utf-8')
def send_pi_data(data):
    try:    
        lora.send_data(data, len(data), lora.frame_counter)
        lora.frame_counter += 1

        print('Data sent!')
        time.sleep(0.5)
    except Exception as e:
        print("error nih " + str(e))

while True:
    try:

        send_pi_data(data_pkt)
    except Exception as e:
        print("error" + str(e))
    
    time.sleep(10)
